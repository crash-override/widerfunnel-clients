/*$('head').append('<link href="http://projects.widerfunnel.com/CLIENTS/dan-dev/style.css" type="text/css" rel="stylesheet">');
$(function(){var fileRef = document.createElement('script'); fileRef.setAttribute("type","text/javascript"); fileRef.setAttribute("src", "http://projects.widerfunnel.com/CLIENTS/dan-dev/wf-script.js"); document.body.appendChild(fileRef);});*/

var WF = {};
WF.exp = {
	
	//Add custom member variables for specific projects here
	dockette: 'CLI-FE4.1',
	variant: 'B',


	// Add specific CSS or hide elements
	addCssProps: function(){
		$('head').append('<style>@import url(http://fonts.googleapis.com/css?family=Raleway:400,300,600);body{width:100%!important;margin:0!important;padding:0!important}div#main-wrap,div#main-wrap *{box-sizing:border-box; -moz-box-sizing:border-box}#main-wrap{width:980px!important}.wf-tagline{width:100%;height:135px;display:table;border-top:none;border-bottom:none;text-align:center;background:#dedfdc}.wf-tag-inner{background:#eeefec;width:980px;height:135px;display:table;margin:0 auto}.wf-tagline h6{font-size:30px;font-weight:400;display:block;margin:50px auto 15px;font-family:Verdana,Tahoma,sans-serif;color:#596257}.wf-tagline p{font-size:17px;font-family:Verdana,Tahoma,sans-serif;line-height:22px;padding:0;margin:0;color:#596257}#DoorStyle,.door-selector{display:block;width:100%;overflow:hidden;height:1px;opacity:0}.wf-doorstyles{display:table;width:760px;margin:0 auto}.wf-door-item{width:81px;float:left;display:block;margin:0 0 0 32px;color:#596257;text-decoration:none}.wf-door-item:first-child{margin-left:0}.wf-door-item span{background:url(//cdn.optimizely.com/img/35583784/dc5f33d389b646b3b819031c1a56aecd.png) 0 0 no-repeat;width:81px;height:131px;display:block;border:3px solid #fff}.wf-door-item.active span{border:3px solid #92c75e}.wf-door-item p,.wf-door-item:hover p{text-align:center;font-family:Arial,sans-serif;font-size:16px;color:#596257;text-decoration:none}.wf-rockford span{background-position:0 -320px}.wf-dayton span{background-position:0 -481px}.wf-carlton span{background-position:0 -644px}.wf-cambridge span{background-position:0 -808px}.wf-mendota span{background-position:0 -970px}.wf-fairmont span{background-position:0 -163px}.wf-austin span{background-position:0 0}.wf-rockford.active span{background-position:-85px -320px}.wf-dayton.active span{background-position:-85px -481px}.wf-carlton.active span{background-position:-85px -644px}.wf-cambridge.active span{background-position:-85px -808px}.wf-mendota.active span{background-position:-85px -970px}.wf-fairmont.active span{background-position:-85px -163px}.wf-austin.active span{background-position:-85px 0}.wf-door-detail-cap{width:100%;height:20px}.wf-door-customize{display:block;width:100%;background:#676766;height:325px;overflow:hidden}.wf-door-img{float:left;width:235px;height:100%}.wf-door-img img{float:right;margin-top:22px;width:175px!important}.wf-door-detail{float:left;width:340px;height:100%;padding:27px 45px 0}#main-wrap .wf-door-detail h2{float:none;text-align:left;color:#fff;font-weight:100;font-size:24px;padding:0;margin:0 0 5px;text-transform:none!important;letter-spacing:1px}#main-wrap .wf-door-detail ul{list-style:none;margin:0;padding:0}#main-wrap .wf-door-detail ul li{font-size:12px;color:#fff;margin:0 0 10px;padding:0;font-family:Helvetica;font-weight:100}#main-wrap .wf-door-detail ul li::before{content:"-  ";color:#fff}a.wf-img-mask{width:50px;height:auto;display:block;overflow:hidden;float:left;text-decoration:none;margin:3px 5px 0}a.wf-img-mask:first-child{margin:3px 5px 0 0}a.wf-img-mask.active:first-child{margin:0 5px 0 0}a.wf-img-mask.active{width:50px;margin:0 5px}a.wf-img-mask span{width:38px;height:38px;border:2px solid #fff;border-radius:38px;margin:0 auto;display:block;overflow:hidden}a.wf-img-mask.active span{width:44px;height:44px;border-radius:44px;border:5px solid #88c343}a.wf-img-mask span img{width:60px;height:60px;margin:-10px 0 0 -10px}a.wf-img-mask p{text-align:center;text-decoration:none;color:#fff;margin:10px 0 0}a.wf-img-mask.active p{color:#88c343;margin:7px 0 0}.wf-door-color small,.wf-door-detail small{color:#999;font-size:10px;font-family:Arial,sans-serif;display:block}.wf-door-color small{margin:20px 0 6px}.wf-door-color{float:left;width:365px;height:100%;overflow:hidden;padding:7px 0 0;position:relative}.wf-designer-paint,.wf-standard-paint{display:table;width:100%}.wf-arrow_box{position:relative;background:#676766;top:20px}.wf-arrow_box:after{bottom:0;left:0;border:solid transparent;content:" ";height:0;width:0;position:absolute;pointer-events:none;border-color:rgba(103,103,102,0);border-bottom-color:#676766;border-width:20px}.wf-arrow_box.door_hi42{margin-left:120px}.wf-arrow_box.door_hi43{margin-left:232px}.wf-arrow_box.door_hi44{margin-left:346px}.wf-arrow_box.door_hi45{margin-left:458px}.wf-arrow_box.door_hi46{margin-left:570px}.wf-arrow_box.door_hi47{margin-left:684px}.wf-arrow_box.door_hi48{margin-left:796px}.wf-step{display:table;width:100%}.wf-step.wf-step-1{margin:20px 0 15px}.wf-step.wf-step-2{margin:40px 0 25px}.wf-step i{background:url(//cdn.optimizely.com/img/35583784/81f9f64c31524c11a2eefeee2d947c4e.png) 0 0 no-repeat;width:53px;height:43px;dipsplay:block;overflow:hidden;float:left;padding:0 10px 0 0}.wf-step p{float:left;font-weight:100;color:#596257;font-size:18px;line-height:43px;margin:0;padding:0;width:100%;position:relative}.wf-step.wf-step-3{margin-top:20px}.wf-step.wf-step-1 i{background-position:0 0}.wf-step.wf-step-2 i{background-position:0 -74px}.wf-step.wf-step-3 i{background-position:0 -147px}.wf-accessories{width:100%;display:table}.wf-pull-left{width:100%;display:table;float:none}.wf-acc-sidebar{width:auto;background:#fff;padding:25px 0;display:table;margin:0 0 0 100px}.wf-acc-sidebar a.wf-sidebar-heading{display:table;color:#596257;float:left;margin-left:33px;text-align:center;text-decoration:none}.wf-acc-sidebar a.wf-sidebar-heading:first-child{margin-left:0}.wf-acc-sidebar a.wf-sidebar-heading.active span{border:3px solid #96c55d}.wf-acc-sidebar a.wf-sidebar-heading span{border:3px solid #fff;margin-bottom:10px}.wf-sidebar-heading span{display:block;background:url(//cdn.optimizely.com/img/35583784/57dd3d6690eb4a75a195fe3411bf7e6a.png) 0 0 no-repeat;width:100px;height:100px}.wf-sidebar-heading.Wall-Cabinets span{background-position:0 0;width:105px;height:132px}.wf-sidebar-heading.Furniture-Cabinets span{background-position:-131px 0;width:105px;height:132px}.wf-sidebar-heading.Base-Cabinets span{background-position:-261px 0;width:105px;height:132px}.wf-sidebar-heading.Tall-Cabinets span{background-position:-417px 0;width:55px;height:131px}.wf-sidebar-heading.Vanity-Cabinets span{background-position:-520px 0;width:105px;height:132px}.wf-sidebar-heading.Accessories span{background-position:-669px 1px;width:72px;height:132px}#cap-Wall-Cabinets{margin-left:131px}#cap-Furniture-Cabinets{margin-left:270px}#cap-Base-Cabinets{margin-left:408px}#cap-Tall-Cabinets{margin-left:520px}#cap-Vanity-Cabinets{margin-left:650px}#cap-Accessories{margin-left:765px}.wf-acc-sidebar ul{display:none;width:100%;padding:0 0 5px 40px;list-style:none;margin:10px 0 0}.wf-acc-sidebar ul.active{display:block}.wf-acc-sidebar ul li{display:table;width:100%;padding:0;margin:0}.wf-acc-sidebar ul li a{display:block;width:100%;color:#c8cccd;text-decoration:none;line-height:25px}.wf-acc-sidebar ul li a.active{color:#96c55d}.wf-prod-item{display:table;width:100%;position:relative;margin-bottom:45px}.wf-acc-list{width:100%;height:auto;display:table;padding:0 100px;background:#f0f0f0}.wf-acc-list h4{color:#596257;border-bottom:1px solid #596257;font-size:16px;padding-bottom:8px;padding-top:0;margin-top:0}.wf-acc-list .wf-prod-img{padding:0;float:left;width:145px}.wf-acc-list .wf-prod-img img{width:145px!important;height:auto!important}.wf-acc-list .wf-prod-desc{width:260px;float:left}.wf-acc-list .wf-order-actions{width:320px;float:right}.wf-row{float:right}.wf-product-width{float:left}.wf-product-end,.wf-product-hinge,.wf-product-qty{float:left;margin-left:10px}.wf-row small{display:block;color:#596257;font-size:11px;padding-bottom:5px}.wf-product-width span{display:block;float:none;color:#969696;font-size:10px;padding-top:10px}.wf-item-actions{position:absolute;bottom:0;right:0}a.wf-continue-step-1,a.wf-continue-to-cart{border-radius:3px;margin-right:10px;line-height:30px;color:#fff!important;text-decoration:none;font-size:14px;text-align:center;background:#92c75e;display:block;float:left;height:30px;width:120px;-webkit-box-shadow:0 2px 0 0 rgba(127,127,127,1);-moz-box-shadow:0 2px 0 0 rgba(127,127,127,1);box-shadow:0 2px 0 0 rgba(127,127,127,1)}a.wf-continue-step-1{float:none;position:absolute;bottom:20px;right:0}a.wf-continue-to-cart{text-decoration:none;float:right;line-height:30px}a.wf-add-quote{border-radius:3px;width:120px;height:30px;line-height:30px;color:#fff!important;text-decoration:none;font-size:14px;text-align:center;background:#349ed1;display:block;float:left;-webkit-box-shadow:0 2px 0 0 rgba(127,127,127,1);-moz-box-shadow:0 2px 0 0 rgba(127,127,127,1);box-shadow:0 2px 0 0 rgba(127,127,127,1)}a.wf-continue-to-cart:hover{background:#a4e069}a.wf-add-quote:hover{background:#3dace1}.wf-row input,.wf-row select{line-height:20px!important;height:20px!important}.wf-contact-mini{padding:40px 0;background:#c5eeae;margin:20px 0 0;border-radius:3px;display:table;width:100%}.wf-contact-mini h6{font-size:18px;color:#596257;padding:0 0 12px;margin:0 auto;text-align:center}.wf-contact-mini hr{height:1px;border:none;border-bottom:1px solid #afd29d;background:0 0;width:100px;margin:0 auto 16px}.wf-contact-mini .wf-contact-img{width:50px;height:50px;background:url(//cdn.optimizely.com/img/35583784/18e6280b8ee24c01bf845cc27cd59f36.png) 0 0 no-repeat;float:left;margin:0 15px 0 30px}.wf-contact-mini p{font-size:14px;color:#596257;float:left;margin:9px 0 0;padding:0;line-height:12px}.wf-contact-mini p b{font-size:18px;color:#596257;display:block}.wf-contact-mini p a{font-size:14px;color:#596257;text-decoration:underline}#leftside,#right-item,div.Dispositif3{opacity:0;width:100%;height:0;margin:0;padding:0;overflow:hidden}.wf-item-added,.wf-no-item{position:absolute;color:#92c75e;bottom:40px;right:20px;text-align:right}.wf-no-item{color:#ff9494;top:-4px;right:145px}.wf-step.wf-step-1 a,.wf-step.wf-step-2 a{color:#92c75e;text-decoration:none}.edit-step{margin-left:10px}.wf-sub-category{display:table;width:100%;background:#f0f0f0;padding:25px 0 30px}.wf-sub-category ul{display:table;width:100%;padding:0 100px}.wf-sub-category ul li{float:left;display:block;border:1px solid #676766;margin:0 0 0 -1px;padding:0}.wf-sub-category ul li a{display:block;width:100%;padding:0 12px;line-height:30px;color:#676766;text-decoration:none;margin:0}.wf-sub-category ul li a.active{background:#676766;color:#fff}.wf-selection-summary{display:table;width:100%}.wf-selection-summary-img{float:left;width:130px;height:120px;overflow:hidden;border-bottom:1px solid #505050}.wf-selection-summary-img img{display:block;margin:7px auto 0;width:85px!important;height:auto!important}.wf-selection-detail{float:left}.wf-selection-detail small{display:block;margin-top:10px;color:#596257;font-size:10px}.wf-selection-detail p{display:block;color:#596257;font-size:18px;padding:0;margin:5px 0}.wf-acc-cap{width:100%;height:20px}.wf-arrow_box-two{position:relative;background:#f0f0f0;top:20px}.wf-arrow_box-two:after{bottom:0;left:0;border:solid transparent;content:" ";height:0;width:0;position:absolute;pointer-events:none;border-color:rgba(103,103,102,0);border-bottom-color:#f0f0f0;border-width:20px}#main-wrap .wf-door-detail h2,.wf-acc-list h4,.wf-acc-sidebar a.wf-sidebar-heading,.wf-door-item p,.wf-step p,.wf-sub-category ul li,.wf-tag-inner h6,a.wf-add-quote,a.wf-continue-step-1,a.wf-continue-to-cart{font-family:Raleway,sans-serif!important}.wf-tag-inner h6{font-weight:400}</style>');
	},

	//Modify Content
	editContent: function(){
		$(document).ready(function(){
				
			$('#main-wrap').before('<div class="wf-tagline"><div class="wf-tag-inner">'
			+	'<h6>Create Your Custom Quote</h6>'
			+'</div></div>');
			
			$('.Dispositif3').before('<div class="wf-step wf-step-1">'
				+	'<p><i></i>Select a door style and finish</p>'
				+'</div>'
				+'<div class="wf-doorstyles">'
				+	'<a href="#" class="wf-door-item wf-rockford" data-door="door_hi42">'
				+		'<span></span>'
				+		'<p>Rockford</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-dayton" data-door="door_hi43">'
				+		'<span></span>'
				+		'<p>Dayton</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-carlton" data-door="door_hi44">'
				+		'<span></span>'
				+		'<p>Carlton</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-cambridge" data-door="door_hi45">'
				+		'<span></span>'
				+		'<p>Cambridge</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-mendota" data-door="door_hi46">'
				+		'<span></span>'
				+		'<p>Mendota</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-fairmont" data-door="door_hi47">'
				+		'<span></span>'
				+		'<p>Fairmont</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-austin" data-door="door_hi48">'
				+		'<span></span>'
				+		'<p>Austin</p>'
				+	'</a>'
				+'</div>'
				+'<div class="wf-door-detail-cap"><div class="wf-arrow_box"></div></div>'
				+'<div class="wf-door-customize">'
				+	'<div class="wf-door-img"></div>'
				+	'<div class="wf-door-detail">'
				+		'<small>Your selected door</small>'
				+		'<h2></h2>'
				+		'<ul></ul>'
				+	'</div>'
				+	'<div class="wf-door-color">'
				+		'<small>Standard Paint</small>'
				+		'<div class="wf-standard-paint"></div>'
				+		'<div class="wf-designer-paint"></div>'
				+	'<a href="#" class="wf-continue-step-1">Continue</a>'
				+	'</div>'
				+'</div>'
				+'<div class="wf-step wf-step-2">'
				+	'<p><i></i>Select your cabinets and accessories</p>'
				+'</div>'
				+'<div class="wf-accessories">'
				+	'<div class="wf-pull-left">'	
				+	'<div class="wf-acc-sidebar"></div>'
				+		'<div class="wf-acc-cap"><div class="wf-arrow_box-two"></div></div>'			
				+		'<div class="wf-sub-category"></div>'
				+	'</div>'
				+	'<div class="wf-acc-list"></div>'
				+'</div>'
				+'<div class="wf-step wf-step-3">'
				+	'<p><i></i>Finalize your quote</p>'
				+'</div>');

				$('#DoorStyle .spacing img').each(function(){
					var triggerID 	= $(this).attr('id');
					var src			= $(this).attr('src');
					var regEx		= new RegExp('_on', 'gi');
					if(regEx.test(src)){
						$('.wf-door-item[data-door='+triggerID+']').addClass('active');
					} 
				});

				$('img#doorImg').appendTo('.wf-door-img');
				
				$('#category-wrap h4').each(function(){
					var sbHeading = $(this).text();
					var isActive = $(this).next('div').css('display');
					var newClass = sbHeading.replace(' ', '-');
					
					if(isActive == 'block'){
						$('.wf-acc-sidebar').append('<a href="#" class="wf-sidebar-heading active '+newClass+'" data-id="'+newClass+'"><span></span>'+sbHeading+'</a><ul class="mover"></ul>');
						$('.wf-acc-cap').attr('id', 'cap-'+newClass);
					} else {
						$('.wf-acc-sidebar').append('<a href="#" class="wf-sidebar-heading '+newClass+'" data-id="'+newClass+'"><span></span>'+sbHeading+'</a><ul class="mover"></ul>');
					}
					$(this).next('div').children('a').each(function(){
						var href 		= $(this).attr('href').replace(' ', '');
						var thisText 	= $(this).text();
						var thisClass	= $(this).attr('class');
						if(thisClass == 'ClickEntryNAV') { 
							$('.wf-acc-sidebar').find('ul').last().append('<li><a href="'+href+'" class="active">'+thisText+'</a></li>');
						} else { 
							$('.wf-acc-sidebar').find('ul').last().append('<li><a href="'+href+'">'+thisText+'</a></li>');
						}
					});
				});
				$('.wf-sidebar-heading.active').next('ul.mover').clone().appendTo('.wf-sub-category');
				
				updateColorSelection();
				updateDoorSelection();
				
				/* CLick handlers */
				$('a.wf-door-item').live('click', function(e){
					e.preventDefault();
					var a = $(this).attr('data-door');
					$('a.wf-door-item').removeClass('active');
					$(this).addClass('active');
					$('img#'+a).trigger('click');
					updateDoorSelection();
					
					$('.wf-standard-paint').empty();
					$('.wf-designer-paint').empty();
					updateColorSelection();
				});
				$('a.wf-img-mask').live('click', function(e){
					e.preventDefault();
					var thisID = $(this).attr('id');
					$('img[data-color='+thisID+']').trigger('click');
					$('.StandardImg img').each(function(){
						var getColor = $(this).siblings('div').html().replace('<br>', ' ');
						var getColorData = $(this).siblings('div').html().replace('<br>', '-');
						$(this).attr('data-color', getColorData);
					});
					$('a.wf-img-mask').removeClass('active');
					$(this).addClass('active');
					var colorText = $('p', this).html().replace(new RegExp("<br>", "gi"), ' ');;
					$('.wf-door-detail ul li:first-child').html(colorText);	
				});
				$('.wf-acc-sidebar a.wf-sidebar-heading').live('click', function(e){
					e.preventDefault();
					$('a.wf-sidebar-heading').removeClass('active');
					$('.wf-sub-category').empty();
					$(this).next('ul').clone().appendTo('.wf-sub-category');
					$(this).addClass('active');
					var dataId = $(this).attr('data-id');
					$('.wf-acc-cap').attr('id', 'cap-'+dataId);
					$('.wf-sub-category ul').find('li:first-child a').trigger('click');
					
					var b = $(this).text();
					
					if($('#categorynav h4:contains("'+b+'")')){
						$('#categorynav h4:contains("'+b+'")').trigger('click');
						setTimeout( function(){ updateProductList(); }, 1000);
					}
					
				});
				$('.wf-sub-category ul.mover li a').live('click', function(e){
					$('.wf-sub-category ul.mover li a').removeClass('active');
					$(this).addClass('active');
					var b = $(this).text();
					$('#categorynav a:contains("'+b+'")').trigger('click');
					setTimeout(function(){ updateProductList(); }, 1000);
					
				});
				$('.wf-accessories').css('display', 'none');
				$('a.wf-continue-step-1').live('click', function(e){
					e.preventDefault();
					$('.wf-accessories').css('display', 'table');			
					$('.wf-doorstyles').css('display', 'none');
					$('.wf-door-detail-cap').css('display', 'none');
					$('.wf-door-customize').css('display', 'none');
					
					var doorImg 	= $('img#doorImg').attr('src');
					var doorTitle 	= $('.wf-door-detail h2').text();
					var doorColor	= $('.wf-door-detail ul li').first().text();
					
					$('.wf-door-customize').after('<div class="wf-selection-summary">'
						+ '<div class="wf-selection-summary-img"><img src="'+doorImg+'" /></div>' 			
						+ '<div class="wf-selection-detail">'
						+ '<small>Door Style</small>'
						+ '<p>'+doorTitle+'</p>'
						+ '<small>Painted White</small>'
						+ '<p>'+doorColor+'</p>'
						+ '</div>'
						+'</div>');
					$('.wf-step.wf-step-1 p').append('<a href="#" class="edit-step">Edit</a>');
					$('.wf-step.wf-step-2 p').append('<a href="#" class="wf-continue-to-cart">Continue</a>');
					$('.wf-step.wf-step-3 p').append('<a href="#" class="wf-continue-to-cart">Continue</a>');
				});
				
				$('a.edit-step').live('click', function(e){
					e.preventDefault();
					$('.wf-accessories').css('display', 'none');			
					$('.wf-doorstyles').css('display', 'table');
					$('.wf-door-detail-cap').css('display', 'table');
					$('.wf-door-customize').css('display', 'table');
					$('.wf-selection-summary').remove();
					$(this).remove();
					$('.wf-step.wf-step-2 p a.wf-continue-to-cart').remove();
					$('.wf-step.wf-step-3 p a.wf-continue-to-cart').remove();
				});
					
				updateProductList();
				$('.wf-product-qty input').val('1');
				var isQuote = false;
				
				
				$('.wf-add-quote').live('click', function(e){
					e.preventDefault();
					var thisParent =  $(this).closest('.wf-prod-item').attr('data-item');
					var thisButton	= $('#frame > div[data-item='+thisParent+']');
					//$(this).parents('.wf-item-actions').prev('.wf-order-actions').find('input').val('1');
					$('tr:nth-child(3) td input', thisButton).first().trigger('click');
					$('tr:nth-child(3) td input', thisButton).first().css('background','red');
					
					$(this).before('<span class="wf-item-added">Item added</span>');

					setTimeout(function(){
						$('.wf-item-added').remove();
					}, 1000);
					
					isQuote = true;
				});
				
				$('.wf-continue-to-cart').live('click', function(e){
					e.preventDefault();			
					if(isQuote == false){
						$(this).before('<span class="wf-no-item">No items added</span>');
						setTimeout(function(){
							$('.wf-no-item').remove();
						}, 1000);  
					} else if(isQuote == true) {
						// $('tr:nth-child(3) td input').trigger('click');
						window.location.replace('https://www.cliqstudios.com/default.aspx?id=999');
						
					}	
					
				});
				$('.wf-prod-item select').live('change', function(){
					var thisSelect 	= $(this).attr('class');
					thisSelect 		= thisSelect.replace(' ', '.');
					var thisVal		= $(this).val();
					var thisItem 	= $(this).closest('.wf-prod-item').attr('data-item');
					$('#frame > div[data-item='+thisItem+']').find('select.'+ thisSelect).val(thisVal);	
				});
				$('.wf-prod-item input.qty').live('blur', function(e){
					e.preventDefault();
					var val = $(this).val();
					var thisParent =  $(this).closest('.wf-prod-item').attr('data-item');
					$('#frame > div[data-item='+thisParent+']').find('.qty').val(val);
				});		
		});
		function updateProductList(){
			$('.wf-acc-list').empty();
			var uniqueClass = 0;
			$('#frame > div').each(function(){
				$(this).attr('data-item','wf-item-'+ uniqueClass);
				
				var title 		= $('#catalog-title', this).text();
				var prodImg 	= $('tr:nth-child(2) a', this).parents().html();
				var prodDesc 	= $('tr:nth-child(2) ul', this).html();
				var widthSelect	= $('tr:nth-child(2) select.width', this).parents().html();
				var hingeSelect = $('tr:nth-child(2) select.width_select', this).parents().html();
				var endSelect	= $('tr:nth-child(2) select.edge_select', this).parents().html();
				var qtyInput	= $('tr:nth-child(2) input.qty', this).parents().html();
				
				var itemButtons	= $('tr:nth-child(3) td', this); 
				
				$('.wf-acc-list').append('<div class="wf-prod-item" data-item="wf-item-'+uniqueClass+'">'
					+ '<h4>'+ title +'</h4>' 
					+ '<div class="wf-prod-img">'+ prodImg +'</div>'
					+ '<div class="wf-prod-desc"><ul>'+ prodDesc +'</ul></div>'
					+ '<div class="wf-order-actions">'
						+ '<div class="wf-row">'
							+ '<div class="wf-product-width"><small>Measurement</small>'+ widthSelect +'</div>'	
							+ '<div class="wf-product-hinge"><small>Hinge</small>'+ hingeSelect +'</div>'
							+ '<div class="wf-product-end"><small>Finished End</small>'+ endSelect +'</div>'
							+ '<div class="wf-product-qty"><small>Qty</small>'+ qtyInput +'</div>'
						+ '</div>'	
					+ '</div>'
					+ '<div class="wf-item-actions">'
					+	'<a href="#" class="wf-add-quote">Add to quote</a>'
					+ '</div>'
				+'</div>');		
				uniqueClass++;
			});
		}
		function updateColorSelection(){
			$('#StandardList .StandardImg img').each(function(){
				var getColor = $(this).siblings('div').html().replace('<br>', ' ');
				var getColorData = $(this).siblings('div').html().replace('<br>', '-');
				$(this).attr('data-color', getColorData);
				var imgSrc  = $(this).attr('src');
				var regEx	= new RegExp('_on', 'gi');
				if(regEx.test(imgSrc)){
					imgSrc = imgSrc.replace('_on', '_off');
					$('.wf-standard-paint').append('<a href="#" class="wf-img-mask active" id="'+getColorData+'">'
					+	'<span><img src="'+imgSrc+'" /></span><p>'+getColor+'</p></a>');
				} else {
					$('.wf-standard-paint').append('<a href="#" class="wf-img-mask" id="'+getColorData+'">'
					+	'<span><img src="'+imgSrc+'" /></span><p>'+getColor+'</p></a>');				
				}
			});
			
			if($('#designerpaint').children().length > 0){
				$('#designerpaint .StandardImg img').each(function(){
					var getColor = $(this).siblings('div').html().replace('<br>', ' ');
					var getColorData = $(this).siblings('div').html().replace('<br>', '-');
					$(this).attr('data-color', getColorData);
					var imgSrc  = $(this).attr('src');
					var regEx	= new RegExp('_on', 'gi');
					if(regEx.test(imgSrc)){
						imgSrc = imgSrc.replace('_on', '_off');
						$('.wf-designer-paint').append('<a href="#" class="wf-img-mask active" id="'+getColorData+'">'
								+	'<span><img src="'+imgSrc+'" /></span><p>'+getColor+'</p></a>');	
					} else {
						$('.wf-designer-paint').append('<a href="#" class="wf-img-mask" id="'+getColorData+'">'
							+	'<span><img src="'+imgSrc+'" /></span><p>'+getColor+'</p></a>');
					}	
				});
				if(!$('small.has-designer').length > 0){
					$('.wf-designer-paint').before('<small class="has-designer">Designer Paint</small>');
				}	
			} else {
				$('small.has-designer').remove();
			}			
		}
		
		function updateDoorSelection(){
			var arrowPos = $(document).find('.wf-door-item.active').attr('data-door');
			$('.wf-arrow_box').attr('class', 'wf-arrow_box '+arrowPos);
			$('.wf-door-detail h2').empty();
			$('.wf-door-detail ul').empty();
			var serialVal = $('#serial_value').text();
			$('.wf-door-detail h2').append(serialVal);
			var colorVal = $('#color_value').text();
			$('.wf-door-detail ul').append('<li>'+colorVal+'</li>');
			$('ul#desc_value li').each(function(){ 
				var detailList = $(this).clone();
				$(detailList).appendTo('.wf-door-detail ul') 
			});
		}
	},
		
	init: function(){
		 var foo = $('#serial_value').text();
		    if(foo != ''){
		         this.addCssProps();
		         this.editContent(); 
		       } else {
		         setTimeout(function(){	
		           WF.exp.init();
		        }, 500);
		     }
		
		
	}	
};  
WF.exp.init();