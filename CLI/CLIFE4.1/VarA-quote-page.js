var WF = {};
WF.exp = {
	
	//Add custom member variables for specific projects here
	dockette: 'CLI-FE4.1',
	variant: 'A',


	// Add specific CSS or hide elements
	addCssProps: function(){
		$('head').append('<style>@import url(http://fonts.googleapis.com/css?family=Raleway:400,300,600);body{width:100%!important;margin:0!important;padding:0!important}.wf-tagline{width:100%;height:200px;display:table;border-top:none;border-bottom:none;text-align:center;background:#dedfdc}.wf-tag-inner{background:#eeefec;width:980px;height:200px;display:table;margin:0 auto}.wf-tagline h6{font-size:30px;font-weight:400;display:block;margin:60px auto 15px;color:#596257}.wf-tagline p{font-size:17px;line-height:22px;padding:0;margin:0;color:#596257}.wf-step{display:table;width:980px}.wf-step.wf-step-1{margin:20px auto 15px}.wf-step.wf-step-2{margin:40px auto 25px}.wf-step.wf-step-1 a,.wf-step.wf-step-2 a{color:#92c75e;text-decoration:underline}.wf-step i{background:url(//cdn.optimizely.com/img/35583784/81f9f64c31524c11a2eefeee2d947c4e.png) 0 0 no-repeat;width:43px;height:43px;dipsplay:block;overflow:hidden;float:left}.wf-step p{float:left;font-weight:100;color:#596257;font-size:18px;line-height:43px;margin:0;padding:0 0 0 10px}.wf-quote-name input{font-family:Myriad Pro,Verdana,Tahoma,sans-serif;color:#c0c2bf;font-size:18px;line-height:18px;font-weight:100}.wf-quote-name{display:block;float:left}.wf-quote-name input{padding:0 0 0 4px;margin:9px 0 0 10px}.wf-quote-name input:disabled{border:none!important;background:none!important;margin:11px 0 0 10px}.wf-step a.wf-edit-name,.wf-step a.wf-new,.wf-step a.wf-print,.wf-step a.wf-save{color:#92c75e;text-decoration:underline;float:left;font-family:Myriad Pro,Verdana,Tahoma,sans-serif;font-weight:100;font-size:18px;line-height:43px;margin:0 0 0 10px}.wf-step a.wf-new{float:right;padding-right:90px;}.wf-step.wf-step-1 i{background-position:0 0}.wf-step.wf-step-2 i{background-position:0 -74px}.wf-step.wf-step-3 i{background-position:0 -147px;margin-left:10px}.Dispositif3 .wf-hide-stuff,.Spacing06.width06.OnTheLeft02,.color_brown01,.color_brown02,.hight04.color_blue01{height:0!important;opacity:0!important;overflow:hidden!important;display:block!important;margin:0!important;padding:0!important}.Spacing10.color_blue02{display:none}.Dispositif3{background:#f0f0f0;padding:15px 25px 25px!important}.quotebox1,.quotebox2{line-height:32px;height:32px;font-size:12px!important;color:#8a8a8a;display:table;width:100%;margin:0!important;padding:0!important}.quotebox1>div,.quotebox2>div{float:left;display:block!important;color:#8a8a8a!important}.quotebox1>div.quotebox_11,.quotebox1>div.quotebox_12,.quotebox2>div.quotebox_11,.quotebox2>div.quotebox_12{display:none!important}.quotebox1>div.quotebox_01,.quotebox2>div.quotebox_01{padding:7px 0 0 5px;text-align:left}.quotebox1>div.quotebox_10,.quotebox2>div.quotebox_10{float:right;padding:7px 5px 0 0;text-align:right}.quotebox1>div.quotebox_08,.quotebox2>div.quotebox_08{margin-top:5px}.quotebox_09{text-align:right!important;width:90px!important;padding-left:12px}.hight07{height:34px}#k_1_1>div{font-weight:400;font-size:16px;color:#596257}#k_1_2,#k_5_2,#k_6_2{width:6px!important;padding-left:40px;padding-top:14px!important;color:#596257!important;font-weight:400!important}#k_1_3,#k_5_3,#k_6_3{float:right!important;text-align:right!important;padding-right:90px;padding-top:14px!important;color:#596257!important;font-weight:400!important}#k_5_1,#k_6_1{color:#92c75e!important;font-size:16px!important;font-style:normal!important;font-weight:400!important}#k_5_1 span.text21,#k_6_1 span.text21{color:#92c75e!important;font-size:12px!important;font-style:normal!important;font-weight:400!important}#k_6_1,#k_6_1 span.text21{color:#596257!important}.width07.OnTheLeft02.text19.Spacing02{font-size:12px;width:auto;padding-left:72px;text-decoration:underline}.wf-quote-value-prop{width:300px;background:#fff;text-align:center;float:left;overflow:hidden}.wf-quote-value-prop h1{width:100%;text-align:center;color:#92c75e;font-size:48px;font-weight:700;height:auto!important;margin:0;padding:20px 0 15px}.wf-quote-value-prop h2{width:100%;text-align:center;color:#92c75e;font-size:24px;font-weight:400;height:auto!important;margin:0;padding:0 0 12px}.wf-quote-value-prop p{width:100%;text-align:center;color:#596257;font-size:11px;margin:0;padding:0 0 20px}.wf-quote-total{float:right;width:400px;display:table}a.wf-submit{border-radius:3px;margin-right:10px;line-height:30px;color:#fff!important;text-decoration:none;font-size:14px;text-align:center;background:#92c75e;display:block;float:left;height:30px;width:120px;-webkit-box-shadow:0 2px 0 0 rgba(127,127,127,1);-moz-box-shadow:0 2px 0 0 rgba(127,127,127,1);box-shadow:0 2px 0 0 rgba(127,127,127,1);position:absolute;right:25px;bottom:25px}.wf-title-label{float:left;margin:0;padding:0;text-align:right;color:#596257;font-size:18px;width:180px}.wf-price-sign{float:left;margin:0;color:#596257;font-size:18px;padding:0 0 0 39px}#div_total_price{float:right;margin:0;padding:0 90px 0 0;color:#596257;font-size:18px;font-weight:700;text-align:right;}#kitchen_quote_item{border-top:2px solid #fff;border-bottom:2px solid #fff;margin-top:12px}.quotebox1,.quotebox2{border:none!important}.quotebox2{background:#fff!important}#line2_second{display:none!important}#main-wrap .wf-door-detail h2,.wf-acc-list h4,.wf-contact-mini h6,.wf-quote-value-prop h1,.wf-quote-value-prop h2,.wf-step p,.wf-tagline h6,.wf-tagline p,a.wf-add-quote,a.wf-submit,a.wf-view-quote{font-family:Raleway,sans-serif!important}#k_5_2, #k_5_3 { color: #92c75e !important; }.wf-selection-add-in	{ width: 100%; display: table; }.wf-selection-add-in p { float: left; padding-left: 100px; color:#7c7d7d !important;}.wf-selection-add-in p:first-child	{ padding-left: 100px; }.wf-selection-add-in p.wf-last { float: right; padding-right: 90px; }.wf-add-more-row { display: table; width:100%; }.wf-add-more-row a { padding: 10px 0 0 87px; color: #302f2f !important; display: block; }</style>');
	},

	//Modify Content
	editContent: function(){
		$(document).ready(function(){
			
			
			$('.Dispositif3').before('<div class="wf-tagline"><div class="wf-tag-inner">'
			+	'<h6>Cabinet Pricing</h6>'
			+	'<p>Select a door style and color from the options below,</p>'
			+	'<p>then begin adding items to your quote.</p>'
			+'</div></div>'
			+'<div class="wf-step wf-step-1">'
			+	'<i></i><p>Select a door style and finish <a href="https://www.cliqstudios.com/kitchen-cabinet-catalog">Edit</a></p>'
			+'</div>'
			+'<div class="wf-step wf-step-2">'
			+	'<i></i><p>Select your cabinets and accessories <a href="https://www.cliqstudios.com/kitchen-cabinet-catalog">Edit</a></p>'
			+'</div>');
			
      setTimeout(function(){
        var doorTitle 	= $('#serial_value').text();
        var doorColor 	= $('#color_value').text();	
        var quoteDate	= $('#current_date').text();	
        
        $('.Dispositif3').prepend('<div class="wf-step wf-step-3">'
        +	'<i></i><p>Finalize your Quote: </p><span class="wf-quote-name"></span>'
        +	'<a href="#" class="wf-edit-name">Edit name</a>'
        +	'<a href="#" class="wf-save">Save</a></p>'
        +	'<a href="#" class="wf-print">Print</a></p>'
        +	'<a href="#" class="wf-new">Start new quote</a>'
        +'</div>'
        +'<div class="wf-selection-add-in">'
        +	'<p>Style: <b>'+doorTitle+'</b></p>'
        +	'<p>Color: <b>'+doorColor+'</b></p>'
        +	'<p class="wf-last">Quote date: <b>'+quoteDate+'</b></p>'
            +'</div>');
        
        $('.Dispositif3 > table').wrap('<div class="wf-hide-stuff"></div>');
      }, 500);
			
			
			var random = Math.random() * (99999 - 10000) + 10000;
			random = Math.round(random);
			$('.Dispositif3').find('input#Quote_Name_1').val('My Quote '+random);
			$('.Dispositif3').find('input#Quote_Name_1').attr('disabled', 'disabled');
			$('.Dispositif3').find('input#Quote_Name_1').appendTo('span.wf-quote-name');
			
			var dealTitle = $('.width04.OnTheLeft02.text22.Spacing02 > div').text();
			var dealTag	  = $('.width09.OnTheLeft02.text19 > div').text();
			var dealText  = $('.width17.OnTheLeft02.Spacing02.Spacing14.text25 > div').text();
			
			$('.Dispositif3').append('<div class="wf-quote-value-prop">'
			+	'<h1>'+dealTitle+'</h1>'
			+	'<h2>'+dealTag+'</h2>'
			+	'<p>'+dealText+'</p>'
			+'</div>'
			+'<div class="wf-quote-total">'
				+'<p class="wf-title-label">Total:</p><p class="wf-price-sign">$</p>'
			+'</div>'
			+	'<a href="#" class="wf-submit">Place order</a>');	
			$('#div_total_price').appendTo('.wf-quote-total');
		
			$('#kitchen_quote_item').after('<div class="wf-add-more-row"><a href="http://www.cliqstudios.com/default.aspx?id=71">+ Add more Items</a></div>');
			
			$('a.wf-edit-name').live('click', function(e){
				e.preventDefault();
				$('.wf-quote-name input').removeAttr('disabled')
			});
			$('input#Quote_Name_1').live('blur', function(e){
				$('.wf-quote-name input').attr('disabled', 'disabled');
			});
			
			$('a.wf-save').live('click', function(e){
				e.preventDefault();
				$('img#imgSaveQuote_1').trigger('click');
			});
			$('a.wf-print').live('click', function(e){
				e.preventDefault();
				$('img#img_print_quote').trigger('click');
			});
			$('a.wf-new').live('click', function(e){
				e.preventDefault();
				$('img#img_start_project').trigger('click');
			});
			$('a.wf-submit').live('click', function(e){
				e.preventDefault();
				$('img#img_place_quote').trigger('click');
			});
			
		});
	},
		
	init: function(){
		this.addCssProps();
		this.editContent(); 
	}	
};  
WF.exp.init();