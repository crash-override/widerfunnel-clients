/*$('head').append('<link href="http://projects.widerfunnel.com/CLIENTS/dan-dev/style.css" type="text/css" rel="stylesheet">');
$(function(){var fileRef = document.createElement('script'); fileRef.setAttribute("type","text/javascript"); fileRef.setAttribute("src", "http://projects.widerfunnel.com/CLIENTS/dan-dev/wf-script.js"); document.body.appendChild(fileRef);});*/

var WF = {};
WF.exp = {
	
	//Add custom member variables for specific projects here
	dockette: 'CLI-FE4.1',
	variant: 'B',

	
	// Add specific CSS or hide elements
	addCssProps: function(){
		$('head').append('<style>@import url(http://fonts.googleapis.com/css?family=Raleway:400,300,600);body{width:100%!important;margin:0!important;padding:0!important}.wf-tagline{width:100%;height:200px;display:table;border-top:none;border-bottom:none;text-align:center;background:#dedfdc}.wf-tag-inner{background:#eeefec;width:980px;height:200px;display:table;margin:0 auto}.wf-tagline h6{font-size:30px;font-weight:400;display:block;margin:50px auto 15px;font-family:Verdana,Tahoma,sans-serif;color:#596257}.wf-tagline p{font-size:17px;font-family:Verdana,Tahoma,sans-serif;line-height:22px;padding:0;margin:0;color:#596257}.wf-step{display:table;width:980px;margin:0 auto;padding-left:50px}.wf-step.wf-step-1,.wf-step.wf-step-2,.wf-step.wf-step-3{margin:40px auto 25px}.wf-step.wf-step-1 a,.wf-step.wf-step-2 a{color:#92c75e;text-decoration:underline}.wf-step i{background:url(//cdn.optimizely.com/img/35583784/81f9f64c31524c11a2eefeee2d947c4e.png) 0 0 no-repeat;width:43px;height:43px;dipsplay:block;overflow:hidden;float:left}.wf-step p{float:left;font-weight:100;color:#596257;font-size:18px;line-height:43px;margin:0;padding:0 0 0 10px}.wf-quote-name input{color:#c0c2bf !important;font-size:18px;line-height:18px;font-weight:100}.wf-quote-name{display:block;float:left}.wf-quote-name input{padding:0 0 0 4px;margin:9px 0 0 10px}.wf-quote-name input:disabled{border:none!important;background:none!important;margin:11px 0 0 10px;-webkit-text-fill-color:#c0c2bf}.wf-step a.wf-edit-name,.wf-step a.wf-new,.wf-step a.wf-print,.wf-step a.wf-save{color:#92c75e;text-decoration:underline;float:left;font-family:Myriad Pro,Verdana,Tahoma,sans-serif;font-weight:100;font-size:18px;line-height:43px;margin:0 0 0 10px}.wf-step a.wf-new{float:right;margin-right:130px}.wf-step.wf-step-1 i{background-position:0 0}.wf-step.wf-step-2 i{background-position:0 -74px}.wf-step.wf-step-3 i{background-position:0 -147px;margin-left:10px}.Dispositif3 .wf-hide-stuff,.Spacing06.width06.OnTheLeft02,.color_brown01,.color_brown02,.hight04.color_blue01{height:0!important;opacity:0!important;overflow:hidden!important;display:block!important;margin:0!important;padding:0!important}.Spacing10.color_blue02{display:none}.Dispositif3{width:100%;display:block;margin:0 auto;height:0;overflow:hidden}.Dispositif3 .width05{width:100%!important}hr.wf-border{display:none}.quotebox1,.quotebox2{height:32px;font-size:12px!important;color:#8a8a8a;display:table;width:100%;margin:0!important;padding:0!important}.quotebox1>div,.quotebox2>div{float:left;display:block!important;color:#8a8a8a!important}.quotebox1>div.quotebox_11,.quotebox1>div.quotebox_12,.quotebox2>div.quotebox_11,.quotebox2>div.quotebox_12{display:none!important}.quotebox1>div.quotebox_01,.quotebox2>div.quotebox_01{padding:0;text-align:left}.quotebox1>div.quotebox_10,.quotebox2>div.quotebox_10{float:right;padding:7px 5px 0 0;text-align:right}.quotebox1>div.quotebox_08,.quotebox2>div.quotebox_08{margin-top:5px}#k_1_1,#k_1_2{width:auto!important;float:left!important}#k_1_3{width:auto!important;float:right!important}.quotebox_09{text-align:left!important;padding-left:12px}.hight07{height:34px}#k_1_1>div{font-weight:400;font-size:16px;color:#596257}#k_1_2,#k_5_2,#k_6_2{width:6px!important;padding-left:40px;padding-top:14px!important;color:#596257!important;font-weight:400!important}#k_1_3,#k_5_3,#k_6_3{text-align:left!important;padding-left:2px;padding-top:14px!important;color:#596257!important;font-weight:400!important}#k_5_1,#k_6_1{color:#92c75e!important;font-size:16px!important;font-style:normal!important;font-weight:400!important}#k_5_1 span.text21,#k_6_1 span.text21{color:#92c75e!important;font-size:12px!important;font-style:normal!important;font-weight:400!important}#k_6_1,#k_6_1 span.text21{color:#596257!important}.width07.OnTheLeft02.text19.Spacing02{font-size:12px;width:auto;padding-left:72px;text-decoration:underline}.wf-quote-value-prop{width:300px;background:#fff;text-align:center;float:left;overflow:hidden}.wf-quote-value-prop h1{width:100%;text-align:center;color:#92c75e;font-size:48px;font-weight:700;height:auto!important;margin:0;padding:20px 0 15px}.wf-quote-value-prop h2{width:100%;text-align:center;color:#92c75e;font-size:24px;font-weight:400;height:auto!important;margin:0;padding:0 0 12px}.wf-quote-value-prop p{width:100%;text-align:center;color:#596257;font-size:11px;margin:0;padding:0 0 20px}.wf-quote-total{float:right;width:400px;display:table}a.wf-submit{border-radius:3px;margin:20px 0 0;line-height:30px;color:#fff!important;text-decoration:none;font-size:14px;text-align:center;background:#92c75e;display:block;height:30px;width:120px;-webkit-box-shadow:0 2px 0 0 rgba(127,127,127,1);-moz-box-shadow:0 2px 0 0 rgba(127,127,127,1);box-shadow:0 2px 0 0 rgba(127,127,127,1);float:right}.wf-title-label{float:left;margin:0;padding:0;text-align:right;color:#596257;font-size:18px;width:180px}.wf-price-sign{float:left;margin:0;color:#596257;font-size:18px;padding:0 0 0 39px}#div_total_price{float:left;margin:0;padding:0;color:#596257;font-size:16px;font-weight:700}.wf-total-row .wf-price .wf-symbols { font-weight: bold; }#kitchen_quote_item{border-top:2px solid #fff;border-bottom:2px solid #fff;margin-top:12px}.quotebox1,.quotebox2{border:none!important}.quotebox2{background:#fff!important}#line2_second{display:none!important}.wf-selection-summary{display:table;width:980px;margin:0 auto;padding-left:160px}.wf-selection-summary-img{float:left;width:130px;margin-right:10px;height:120px;overflow:hidden;border-bottom:1px solid #505050}.wf-selection-summary-img img{display:block;margin:7px auto 0;width:85px!important;height:auto!important}.wf-selection-detail{float:left}.wf-selection-detail small{display:block;margin-top:10px;color:#596257;font-size:10px}.wf-selection-detail p{display:block;color:#596257;font-size:18px;padding:0;margin:5px 0}.wf-quote-list{margin:0 auto;width:980px;padding:0 105px;box-sizing:border-box}#kitchen_quote_item .quotebox_01{padding:0;width:33px;height:35px;background:url(//cdn.optimizely.com/img/35583784/05669c7e177346ba814c39f5222acc66.png) 0 0 no-repeat}#kitchen_quote_item .quotebox_01 img{width:32px;-ms-filter:"alpha(Opacity=0)";filter:alpha(opacity=0);opacity:0;height:28px}#kitchen_quote_item .quotebox_06,#kitchen_quote_item .quotebox_07{display:none!important}.wf-cart{background:#fff;margin:0 auto!important;width:980px!important;padding:0 105px 30px 88px!important;box-sizing:border-box}.wf-cart .wf-row{display:table;width:100%;margin-top:30px}.wf-cart .wf-row:first-child{border-top:1px solid #8e8e8e;padding-top:30px}.wf-cart .wf-row.wf-total-row{margin-top:20px;padding-top:20px;border-top:1px solid #8e8e8e}.wf-quote-label-wrap{float:left;padding-left:390px}.wf-price{float:right;display:table}.wf-symbols,.wf-total-total,.wf-totals{float:left}.wf-price,.wf-quote-label-wrap{font-size:16px;font-family:Arial,sans-serif}.wf-row{color:#596257!important}.wf-row.wf-discount-row{color:#92c75e!important}#div_discount{text-indent:-7px;overflow:hidden}p.wf-disclaimer{display:block;float:right;clear:right;margin-top:30px;padding-top:0;font-size:12px;color:#596257;width:370px}.wf-quote-row{display:table;width:100%;margin-bottom:20px}.wf-quote-left{float:left;display:table;width:520px}.wf-quote-left h1{width:auto!important;float:left;height:auto;paddng:0;margin:0;font-size:16px;font-weight:400;color:#596257;line-height:30px}.wf-quote-right{float:right;display:table}.wf-quote-left .quotebox_01{float:left;margin:0 20px 0 0}.wf-quote-left p{display:block;width:100%;clear:left;padding-left:52px}.wf-quote-left a{margin-left:52px;color:#92c75e!important;text-decoration:underline!important}.wf-quote-right input{text-align:left;border:1px solid #a9a9a9;width:35px!important;height:20px!important;display:block;background:#fff!important}.quotebox_09>div{font-size:18px;color:#596257;line-height:25px;width:130px!important;text-align:right!important}.quotebox_09{width:130px!important}.hight05.color_blue03,.quotebox_02,.quotebox_03,.quotebox_04,.quotebox_05,.quotebox_06,.quotebox_07,.quotebox_10{width:0!important;height:0!important;oveflow:hidden!important;display:block!important;padding:0!important;margin:0!important}#quote-warning{display:none!important}.wf-quote-left h1,.wf-quote-name input,.wf-selection-detail p,.wf-step p,.wf-tag-inner h6,a.wf-submit{font-family:Raleway,sans-serif!important}.wf-discount-row .wf-quote-label-wrap {padding-left: 386px;}.wf-discount-row .wf-quote-label-wrap label { text-indent: 0; padding-left: 5px; }a.wf-cart-remove:hover,a.wf-edit-name:hover,a.wf-save:hover,a.wf-print:hover,a.wf-new:hover, .wf-step p a:hover { text-decoration: none !important; }a.wf-submit:hover{background:#a4e069!important;}.wf-tag-inner p a { font-weight: bold; text-decoration: none; color: #596257;}</style>');
	},

	//Modify Content
	editContent: function(){
		$(document).ready(function(){
      setTimeout(function(){    
			var doorImg 	  = $('img#doorImg').attr('src');
			var doorTitle 	= $('#serial_value').text();
			var doorColor 	= $('#color_value').text();	
			
			var dealTitle = $('.width04.OnTheLeft02.text22.Spacing02 > div').text();
			var dealTag	  = $('.width09.OnTheLeft02.text19 > div').text();
			var dealText  = $('.width17.OnTheLeft02.Spacing02.Spacing14.text25 > div').text();
			
			$('.Dispositif3').before('<div class="wf-tagline"><div class="wf-tag-inner">'
			+	'<h6>Create Your Custom Quote</h6>'
      + '<p>Want a professional design and custom quote?</p>'
      + '<p><b>Call 800.576.7930</b> or <a href="http://www.websitealive9.com/2168/rRouter.asp?groupid=2168&amp;websiteid=0&amp;departmentid=2606&amp;javascriptok=false" onclick="javascript:window.open("http://www.websitealive9.com/2168/rRouter.asp?groupid=2168&amp;websiteid=0&amp;departmentid=2606&amp;javascriptok=false","","width=450,height=400"); return false;" target="_blank">Live chat</a> with a designer</p>'                         
			+ '</div></div>'
			+ '<div class="wf-step wf-step-1">'
			+	'<i></i><p>Select a door style and finish <a href="https://www.cliqstudios.com/kitchen-cabinet-catalog">Edit</a></p>'
			+ '</div>'
			+ '<div class="wf-selection-summary">'
			+ '<div class="wf-selection-summary-img"><img src="'+doorImg+'" /></div>' 			
			+ '<div class="wf-selection-detail">'
			+ '<small>Door Style</small>'
			+ '<p>'+doorTitle+'</p>'
			+ '<small>Painted White</small>'
			+ '<p>'+doorColor+'</p>'
			+ '</div>'
			+'</div>'
			+'<div class="wf-step wf-step-2">'
			+	'<i></i><p>Select your cabinets and accessories <a href="https://www.cliqstudios.com/kitchen-cabinet-catalog">Edit</a></p>'
			+'</div>'
			+'<div class="wf-quote-list"></div>'
			+'<div class="wf-step wf-step-3">'
			+	'<i></i><p>Finalize your Quote: </p><span class="wf-quote-name"></span>'
			+	'<a href="#" class="wf-edit-name">Edit name</a>'
			+	'<a href="#" class="wf-save">Save</a></p>'
			+	'<a href="#" class="wf-print">Print</a></p>'
			+	'<a href="#" class="wf-new">Start new quote</a>'
			+'</div>'
			+'<div class="wf-cart">'
			+	'<div class="wf-row">'
			+		'<div class="wf-quote-label-wrap">'
			+			'<label>Sub-Total:</label>'
			+		'</div>'
			+		'<div class="wf-price">'
			+			'<div class="wf-symbols">$</div>'
			+			'<div class="wf-totals wf-subtotal"></div>'
			+		'</div>'
			+	'</div>'
			+	'<div class="wf-row wf-discount-row">'
			+		'<div class="wf-quote-label-wrap">'
			+			'<label>'+dealTitle+'</label>&nbsp;'
			+			'<small>'+dealTag+'</small>'
			+		'</div>'
			+		'<div class="wf-price">'
			+			'<div class="wf-symbols">- $</div>'
			+			'<div class="wf-totals"></div>'
			+		'</div>'
			+	'</div>'
			+	'<div class="wf-row">'
			+		'<div class="wf-quote-label-wrap">'
			+			'<label>Shipping Surcharge</label>'
			+			'<small>(Orders less than $2,000)</small>'
			+		'</div>'
			+		'<div class="wf-price">'
			+			'<div class="wf-symbols">$</div>'
			+			'<div class="wf-totals wf-shipping-price"></div>'
			+		'</div>'
			+	'</div>'
			+	'<div class="wf-row wf-total-row">'
			+		'<div class="wf-quote-label-wrap">'
			+			'<label>Total:</label>'
			+		'</div>'
			+		'<div class="wf-price">'
			+			'<div class="wf-symbols">$</div>'
			+			'<div class="wf-total-total"></div>'
			+		'</div>'
			+	'</div>'
			+	'<a href="#" class="wf-submit">Place order</a>'
			+	'<p class="wf-disclaimer">Note: All prices quoted are valid for 30 days from the original quote date. Prices are Subject to change without notice.</p>'
			+'</div>');
        
        $('#kitchen_quote_item').appendTo('.wf-quote-list');
			$('#quote_price').appendTo('.wf-subtotal');
			$('#div_discount').appendTo('.wf-discount-row .wf-totals');
			$('#div_manual_freight').appendTo('.wf-shipping-price');
			
			$('.wf-quote-list #kitchen_quote_item').children().each(function(){
				var itemTitle 		= $(this).find('.quotebox_02 div').text();
				var itemDimension 	= $(this).find('.quotebox_03 div').text();
				var itemHinge		= $(this).find('.quotebox_04 div').text();
				var itemFinish		= $(this).find('.quotebox_05 div').text();
				
				
				$(this).append('<div class="wf-quote-row"><div class="wf-quote-left">'	
				+	'<h1>'+itemTitle+'</h1>'
				+	'<p>'+itemDimension+', Hinge: '+itemHinge+', End Finish: '+itemFinish+'</p>'
				+	'<a href="#" class="wf-cart-remove">Remove</a>'
				+'</div>'
				+'<div class="wf-quote-right">'	
				+'</div></div>');
			});
			
			$('.wf-quote-list #kitchen_quote_item').children().each(function(){
				var b = $('.quotebox_01', this);
				var c = $('.quotebox_08', this);
				var d = $('.quotebox_09', this);
				
				$('.wf-quote-left', this).prepend(b);
				$('.wf-quote-right', this).append(c);
				$('.wf-quote-right', this).append(d);
			});

			
			$('.wf-cart-remove').live('click', function(e){
				e.preventDefault();
				var a = $(this).parent().parent().parent().find('.quotebox_10').find('img');
				$(a).trigger('click');
			});
			
			var findTotal = $('#div_total_price');
			//console.log('total start text: '+findTotal);
			$('body').find('.wf-total-total').append(findTotal);
			
			$('#kitchen_quote_item input').live('blur', function(){			
				$('.wf-total-total').empty();
				setTimeout(function(){
					var findTotalChange = $('#div_total_price').text();
					$('.wf-total-total').append(findTotalChange);
				}, 500);
			});
			
			
			
			$('.Dispositif3 > table').wrap('<div class="wf-hide-stuff"></div>');
			$('.Dispositif3').prepend('<hr class="wf-border" />');
			
			var random = Math.random() * (99999 - 10000) + 10000;
			random = Math.round(random);
			$('.Dispositif3').find('input#Quote_Name_1').val('My Quote '+random);
			$('.Dispositif3').find('input#Quote_Name_1').attr('disabled', 'disabled');
			$('.Dispositif3').find('input#Quote_Name_1').appendTo('span.wf-quote-name');
				
			
		
			$('a.wf-edit-name').live('click', function(e){
				e.preventDefault();
				$('.wf-quote-name input').removeAttr('disabled')
			});
			$('input#Quote_Name_1').live('blur', function(e){
				$('.wf-quote-name input').attr('disabled', 'disabled');
			});
			
			$('a.wf-save').live('click', function(e){
				e.preventDefault();
				$('img#imgSaveQuote_1').trigger('click');
			});
			$('a.wf-print').live('click', function(e){
				e.preventDefault();
				$('img#img_print_quote').trigger('click');
			});
			$('a.wf-new').live('click', function(e){
				e.preventDefault();
				$('img#img_start_project').trigger('click');
			});
			$('a.wf-submit').live('click', function(e){
				e.preventDefault();
				$('img#img_place_quote').trigger('click');
			});
			}, 500);
		});
	},
		
	init: function(){
			
		this.addCssProps();
		this.editContent(); 
		
		/*	if(){
				
			} else {
			
			}*/

	}	
};  
WF.exp.init();