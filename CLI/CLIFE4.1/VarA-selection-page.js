/*$('head').append('<link href="http://projects.widerfunnel.com/CLIENTS/dan-dev/style.css" type="text/css" rel="stylesheet">');
$(function(){var fileRef = document.createElement('script'); fileRef.setAttribute("type","text/javascript"); fileRef.setAttribute("src", "http://projects.widerfunnel.com/CLIENTS/dan-dev/wf-script.js"); document.body.appendChild(fileRef);});*/

var WF = {};
WF.exp = {
	
	//Add custom member variables for specific projects here
	dockette: 'CLI-FE4.1',
	variant: 'A',


	// Add specific CSS or hide elements
	addCssProps: function(){    
		$('head').append('<style>@import url(http://fonts.googleapis.com/css?family=Raleway:400,300,600);body{width:100%!important;margin:0!important;padding:0!important}div#main-wrap,div#main-wrap *{box-sizing:border-box;-moz-box-sizing:border-box}#main-wrap{width:980px!important}.wf-tagline{width:100%;height:200px;display:table;border-top:none;border-bottom:none;text-align:center;background:#dedfdc}.wf-tag-inner{background:#eeefec;width:980px;height:200px;display:table;margin:0 auto}.wf-tagline h6{font-size:30px;font-weight:400;display:block;margin:60px auto 15px;color:#596257}.wf-tagline p{font-size:17px;line-height:22px;padding:0;margin:0;color:#596257}#DoorStyle,.door-selector{display:block;width:100%;overflow:hidden;height:1px;opacity:0}.wf-doorstyles{display:table;width:760px;margin:0 auto}.wf-door-item{width:81px;float:left;display:block;margin:0 0 0 32px;color:#596257;text-decoration:none}.wf-door-item:first-child{margin-left:0}.wf-door-item span{background:url(//cdn.optimizely.com/img/35583784/dc5f33d389b646b3b819031c1a56aecd.png) 0 0 no-repeat;width:81px;height:131px;display:block;border:3px solid #fff}.wf-door-item.active span{border:3px solid #92c75e}.wf-door-item p,.wf-door-item:hover p{text-align:center;font-family:Arial,sans-serif;font-size:16px;color:#596257;text-decoration:none}.wf-rockford span{background-position:0 -320px}.wf-dayton span{background-position:0 -481px}.wf-carlton span{background-position:0 -644px}.wf-cambridge span{background-position:0 -808px}.wf-mendota span{background-position:0 -970px}.wf-fairmont span{background-position:0 -163px}.wf-austin span{background-position:0 0}.wf-rockford.active span{background-position:-85px -320px}.wf-dayton.active span{background-position:-85px -481px}.wf-carlton.active span{background-position:-85px -644px}.wf-cambridge.active span{background-position:-85px -808px}.wf-mendota.active span{background-position:-85px -970px}.wf-fairmont.active span{background-position:-85px -163px}.wf-austin.active span{background-position:-85px 0}.wf-door-detail-cap{width:100%;height:20px}.wf-door-customize{display:block;width:100%;background:#676766;height:290px;overflow:hidden}.wf-door-img{float:left;width:235px;height:100%}.wf-door-img img{float:right;margin-top:22px;width:175px!important}.wf-door-detail{float:left;width:340px;height:100%;padding:27px 45px 0}#main-wrap .wf-door-detail h2{float:none;text-align:left;color:#fff;font-weight:100;font-size:24px;padding:0;margin:0 0 5px;text-transform:none!important;letter-spacing:1px}#main-wrap .wf-door-detail ul{list-style:none;margin:0;padding:0}#main-wrap .wf-door-detail ul li{font-size:12px;color:#fff;margin:0 0 10px;padding:0;font-family:Helvetica;font-weight:100}#main-wrap .wf-door-detail ul li::before{content:"-  ";color:#fff}a.wf-img-mask{width:50px;height:auto;display:block;overflow:hidden;float:left;text-decoration:none;margin:3px 5px 0}a.wf-img-mask:first-child{margin:3px 5px 0 0}a.wf-img-mask.active:first-child{margin:0 5px 0 0}a.wf-img-mask.active{width:50px;margin:0 5px}a.wf-img-mask span{width:38px;height:38px;border:2px solid #fff;border-radius:38px;margin:0 auto;display:block;overflow:hidden}a.wf-img-mask.active span{width:44px;height:44px;border-radius:44px;border:5px solid #88c343}a.wf-img-mask span img{width:60px;height:60px;margin:-10px 0 0 -10px}a.wf-img-mask p{text-align:center;text-decoration:none;color:#fff;margin:10px 0 0}a.wf-img-mask.active p{color:#88c343;margin:7px 0 0}.wf-door-color small,.wf-door-detail small{color:#999;font-size:10px;font-family:Arial,sans-serif;display:block}.wf-door-color small{margin:20px 0 6px}.wf-door-color{float:left;width:365px;height:100%;overflow:hidden;padding:7px 0 0}.wf-designer-paint,.wf-standard-paint{display:table;width:100%}.wf-arrow_box{position:relative;background:#676766;top:20px}.wf-arrow_box:after{bottom:0;left:0;border:solid transparent;content:" ";height:0;width:0;position:absolute;pointer-events:none;border-color:rgba(103,103,102,0);border-bottom-color:#676766;border-width:20px}.wf-arrow_box.door_hi42{margin-left:120px}.wf-arrow_box.door_hi43{margin-left:232px}.wf-arrow_box.door_hi44{margin-left:346px}.wf-arrow_box.door_hi45{margin-left:458px}.wf-arrow_box.door_hi46{margin-left:570px}.wf-arrow_box.door_hi47{margin-left:684px}.wf-arrow_box.door_hi48{margin-left:796px}.wf-step{display:table;width:100%}.wf-step.wf-step-1{margin:20px 0 15px}.wf-step.wf-step-2{margin:40px 0 25px}.wf-step i{background:url(//cdn.optimizely.com/img/35583784/81f9f64c31524c11a2eefeee2d947c4e.png) 0 0 no-repeat;width:43px;height:43px;dipsplay:block;overflow:hidden;float:left}.wf-step p{float:left;font-weight:100;color:#596257;font-size:18px;line-height:43px;margin:0;padding:0 0 0 10px}.wf-step.wf-step-1 i{background-position:0 0}.wf-step.wf-step-2 i{background-position:0 -74px}.wf-accessories{width:100%;display:table}.wf-pull-left{width:240px;float:left}.wf-acc-sidebar{width:100%;background:#999;padding:25px 0}.wf-acc-sidebar a.wf-sidebar-heading{display:block;width:100%;color:#fff;line-height:30px;padding:0 0 0 30px;margin:0;text-decoration:none}.wf-acc-sidebar a.wf-sidebar-heading.active{color:#999;background:#e6f8e0;border-left:3px solid #96c55d}.wf-acc-sidebar a.wf-sidebar-heading.active:hover,.wf-acc-sidebar a.wf-sidebar-heading:hover{background:#fff;border-left:3px solid #fff;color:#999}.wf-acc-sidebar ul{display:none;width:100%;padding:0 0 5px 40px;list-style:none;margin:10px 0 0}.wf-acc-sidebar ul.active{display:block}.wf-acc-sidebar ul li{display:table;width:100%;padding:0;margin:0}.wf-acc-sidebar ul li a{display:block;width:100%;color:#c8cccd;text-decoration:none;line-height:25px}.wf-acc-sidebar ul li a.active{color:#96c55d}.wf-prod-item{display:table;width:100%;position:relative;margin-bottom:45px}.wf-acc-list{width:720px;height:auto;display:table;padding-left:60px}.wf-acc-list h4{color:#596257;border-bottom:1px solid #596257;font-size:16px;padding-bottom:8px;padding-top:0;margin-top:0}.wf-acc-list .wf-prod-img{padding:0;float:left;width:181px;overflow:hidden}.wf-acc-list .wf-prod-img a{display: block;}.wf-acc-list .wf-prod-desc{width:178px;float:left}.wf-acc-list .wf-order-actions{width:300px;float:right}.wf-row{float:right}.wf-product-width{float:left}.wf-product-end,.wf-product-hinge,.wf-product-qty{float:left;margin-left:10px}.wf-row small{display:block;color:#596257;font-size:11px;padding-bottom:5px}.wf-product-width span{display:block;float:none;color:#969696;font-size:10px;padding-top:10px}.wf-item-actions{position:absolute;bottom:0;right:0}a.wf-add-quote{border-radius:3px;margin-right:10px;line-height:30px;color:#fff!important;text-decoration:none;font-size:14px;text-align:center;background:#92c75e;display:block;float:left;height:30px;width:120px;-webkit-box-shadow:0 2px 0 0 rgba(127,127,127,1);-moz-box-shadow:0 2px 0 0 rgba(127,127,127,1);box-shadow:0 2px 0 0 rgba(127,127,127,1)}a.wf-view-quote{border-radius:3px;width:120px;height:30px;line-height:30px;color:#fff!important;text-decoration:none;font-size:14px;text-align:center;background:#349ed1;display:block;float:left;-webkit-box-shadow:0 2px 0 0 rgba(127,127,127,1);-moz-box-shadow:0 2px 0 0 rgba(127,127,127,1);box-shadow:0 2px 0 0 rgba(127,127,127,1)}a.wf-add-quote:hover{background:#a4e069}a.wf-view-quote:hover{background:#3dace1}.wf-row input,.wf-row select{line-height:20px!important;height:20px!important}.wf-contact-mini{padding:40px 0;background:#c5eeae;margin:20px 0 0;border-radius:3px;display:table;width:100%}.wf-contact-mini h6{font-size:18px;color:#596257;padding:0 0 12px;margin:0 auto;text-align:center}.wf-contact-mini hr{height:1px;border:none;border-bottom:1px solid #afd29d;background:0 0;width:100px;margin:0 auto 16px}.wf-contact-mini .wf-contact-img{width:50px;height:50px;background:url(//cdn.optimizely.com/img/35583784/18e6280b8ee24c01bf845cc27cd59f36.png) 0 0 no-repeat;float:left;margin:0 15px 0 30px}.wf-contact-mini p{font-size:14px;color:#596257;float:left;margin:9px 0 0;padding:0;line-height:12px}.wf-contact-mini p b{font-size:18px;color:#596257;display:block}.wf-contact-mini p a{font-size:14px;color:#596257;text-decoration:underline}#leftside,#right-item,div.Dispositif3{opacity:0;width:100%;height:0;margin:0;padding:0;overflow:hidden}.wf-item-added,.wf-no-item{position:absolute;color:#92c75e;bottom:40px;right:20px;text-align:right}.wf-no-item{color:#ff9494}#main-wrap .wf-door-detail h2,.wf-acc-list h4,.wf-contact-mini h6,.wf-step p,.wf-tagline h6,.wf-tagline p,a.wf-add-quote,a.wf-view-quote{font-family:Raleway,sans-serif!important}.wf-order-actions > span {display: block;float: none;color: #969696;font-size: 10px;padding-top: 10px;clear: right;padding-left: 35px;}.wf-item-added{right: 161px;}</style>');
	},

	//Modify Content
	editContent: function(){
		$(document).ready(function(){
			
			$('#main-wrap').before('<div class="wf-tagline"><div class="wf-tag-inner">'
			+	'<h6>Cabinet Pricing</h6>'
			+	'<p>Select a door style and color from the options below,</p>'
			+	'<p>then begin adding items to your quote.</p>'
			+'</div></div>');
			
			$('.Dispositif3').before('<div class="wf-step wf-step-1">'
				+	'<i></i><p>Select a door style and finish</p>'
				+'</div>'
				+'<div class="wf-doorstyles">'
				+	'<a href="#" class="wf-door-item wf-rockford" data-door="door_hi42">'
				+		'<span></span>'
				+		'<p>Rockford</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-dayton" data-door="door_hi43">'
				+		'<span></span>'
				+		'<p>Dayton</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-carlton" data-door="door_hi44">'
				+		'<span></span>'
				+		'<p>Carlton</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-cambridge" data-door="door_hi45">'
				+		'<span></span>'
				+		'<p>Cambridge</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-mendota" data-door="door_hi46">'
				+		'<span></span>'
				+		'<p>Mendota</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-fairmont" data-door="door_hi47">'
				+		'<span></span>'
				+		'<p>Fairmont</p>'
				+	'</a>'
				+	'<a href="#" class="wf-door-item wf-austin" data-door="door_hi48">'
				+		'<span></span>'
				+		'<p>Austin</p>'
				+	'</a>'
				+'</div>'
				+'<div class="wf-door-detail-cap"><div class="wf-arrow_box"></div></div>'
				+'<div class="wf-door-customize">'
				+	'<div class="wf-door-img"></div>'
				+	'<div class="wf-door-detail">'
				+		'<small>Your selected door</small>'
				+		'<h2></h2>'
				+		'<ul></ul>'
				+	'</div>'
				+	'<div class="wf-door-color">'
				+		'<small>Standard Paint</small>'
				+		'<div class="wf-standard-paint"></div>'
				+		'<div class="wf-designer-paint"></div>'
				+	'</div>'
				+'</div>'
				+'<div class="wf-step wf-step-2">'
				+	'<i></i><p>Select your cabinets and accessories</p>'
				+'</div>'
				+'<div class="wf-accessories">'
				+	'<div class="wf-pull-left">'	
				+	'<div class="wf-acc-sidebar"></div>'
				+		'<div class="wf-contact-mini">'
				+			'<h6>Speak with a designer</h6>'
				+			'<hr />'
				+			'<div class="wf-contact-img"></div>'
				+			'<p>'
				+			'<b>800.576.7930</b><br />'
				+			'or <a href="http://www.websitealive9.com/2168/rRouter.asp?groupid=2168&amp;websiteid=0&amp;departmentid=2606&amp;javascriptok=false" onclick="javascript:window.open(\'http://www.websitealive9.com/2168/rRouter.asp?groupid=2168&amp;websiteid=0&amp;departmentid=2606&amp;javascriptok=false\',\'\',\'width=450,height=400\'); return false;" target="_blank">Live chat</a>'
				+			'</p>'
				+		'</div>'
				+	'</div>'
				+	'<div class="wf-acc-list"></div>'
				+'</div>');
			
				$('#DoorStyle .spacing img').each(function(){
					var triggerID 	= $(this).attr('id');
					var src			= $(this).attr('src');
					var regEx		= new RegExp('_on', 'gi');
					if(regEx.test(src)){
						$('.wf-door-item[data-door='+triggerID+']').addClass('active');
					} 
				});

				$('img#doorImg').appendTo('.wf-door-img');
				
				$('#category-wrap h4').each(function(){
					var sbHeading = $(this).text();
					var isActive = $(this).next('div').css('display');
					if(isActive == 'block'){
						$('.wf-acc-sidebar').append('<a href="#" class="wf-sidebar-heading active">'+sbHeading+'</a><ul class="active"></ul>');
					} else {
						$('.wf-acc-sidebar').append('<a href="#" class="wf-sidebar-heading">'+sbHeading+'</a><ul></ul>');
					}
					$(this).next('div').children('a').each(function(){
						var href 		= $(this).attr('href').replace(' ', '');
						var thisText 	= $(this).text();
						var thisClass	= $(this).attr('class');
						if(thisClass == 'ClickEntryNAV') { 
							$('.wf-acc-sidebar').find('ul').last().append('<li><a href="'+href+'" class="active">'+thisText+'</a></li>');
						} else { 
							$('.wf-acc-sidebar').find('ul').last().append('<li><a href="'+href+'">'+thisText+'</a></li>');
						}
					});				
				});
				
				updateColorSelection();
				updateDoorSelection();
				
				/* CLick handlers */
				$('a.wf-door-item').live('click', function(e){
					e.preventDefault();
					var a = $(this).attr('data-door');
					$('a.wf-door-item').removeClass('active');
					$(this).addClass('active');
					$('img#'+a).trigger('click');
					updateDoorSelection();
					
					$('.wf-standard-paint').empty();
					$('.wf-designer-paint').empty();
					updateColorSelection();
				});
				$('a.wf-img-mask').live('click', function(e){
					e.preventDefault();
					var thisID = $(this).attr('id');
					$('img[data-color='+thisID+']').trigger('click');
					$('.StandardImg img').each(function(){
						var getColor = $(this).siblings('div').html().replace('<br>', ' ');
						var getColorData = $(this).siblings('div').html().replace('<br>', '-');
						$(this).attr('data-color', getColorData);
					});
					$('a.wf-img-mask').removeClass('active');
					$(this).addClass('active');
					var colorText = $('p', this).html().replace(new RegExp("<br>", "gi"), ' ');;
					$('.wf-door-detail ul li:first-child').html(colorText);	
				});
				$('.wf-acc-sidebar a.wf-sidebar-heading').live('click', function(e){
					e.preventDefault();
					$('a.wf-sidebar-heading').removeClass('active');
					$('.wf-acc-sidebar ul').removeClass('active');
					$(this).addClass('active');
					$(this).next('ul').addClass('active');
          
          $(this).next('ul').find('li:first-child a').trigger('click');
					
					var b = $(this).text();
					
					if($('#categorynav h4:contains("'+b+'")')){
						$('#categorynav h4:contains("'+b+'")').trigger('click');
						setTimeout( function(){ updateProductList(); }, 1000);
					}
          
				});
				$('.wf-acc-sidebar ul li a').live('click', function(e){
					$('.wf-acc-sidebar ul li a').removeClass('active');
					$(this).addClass('active');
					var b = $(this).text();
					$('#categorynav a:contains("'+b+'")').trigger('click');
					setTimeout(function(){ updateProductList(); }, 1000);
					
				});
				
				updateProductList();
				$('.wf-product-qty input').val('1');
				var isQuote = false;
				
				
				$('.wf-add-quote').live('click', function(e){
					e.preventDefault();
					var thisParent =  $(this).closest('.wf-prod-item').attr('data-item');
					var thisButton	= $('#frame > div[data-item='+thisParent+']');
					//$(this).parents('.wf-item-actions').prev('.wf-order-actions').find('input').val('1');
					$('tr:nth-child(3) td input', thisButton).first().trigger('click');
					$('tr:nth-child(3) td input', thisButton).first().css('background','red');
					
					$(this).before('<span class="wf-item-added">Item added</span>');

					setTimeout(function(){
						$('.wf-item-added').remove();
					}, 1000);
					
					isQuote = true;
				});
				
				$('.wf-view-quote').live('click', function(e){
					e.preventDefault();			
					if(isQuote == false){
						$(this).before('<span class="wf-no-item">No items added</span>');
						setTimeout(function(){
							$('.wf-no-item').remove();
						}, 1000);  
					} else if(isQuote == true) {
						// $('tr:nth-child(3) td input').trigger('click');
						window.location.replace('https://www.cliqstudios.com/default.aspx?id=999');
						
					}	
					
				});
				$('.wf-prod-item select').live('change', function(){
					var thisSelect 	= $(this).attr('class');
					thisSelect 		= thisSelect.replace(' ', '.');
					var thisVal		= $(this).val();
					var thisItem 	= $(this).closest('.wf-prod-item').attr('data-item');
					$('#frame > div[data-item='+thisItem+']').find('select.'+ thisSelect).val(thisVal);	
				});
				$('.wf-prod-item input.qty').live('blur', function(e){
					e.preventDefault();
					var val = $(this).val();
					var thisParent =  $(this).closest('.wf-prod-item').attr('data-item');
					$('#frame > div[data-item='+thisParent+']').find('.qty').val(val);
				});
	});
		function updateProductList(){
			$('.wf-acc-list').empty();
			var uniqueClass = 0;
			$('#frame > div').each(function(){
				$(this).attr('data-item','wf-item-'+ uniqueClass);
				
				var title 		= $('#catalog-title', this).text();
				var prodImg 	= $('tr:nth-child(2) a', this).parents().html();
				var prodDesc 	= $('tr:nth-child(2) ul', this).html();
				var widthSelect	= $('tr:nth-child(2) select.width', this).parents().html();
				var hingeSelect = $('tr:nth-child(2) select.width_select', this).parents().html();
				var endSelect	= $('tr:nth-child(2) select.edge_select', this).parents().html();
				var qtyInput	= $('tr:nth-child(2) input.qty', this).parents().html();
				
				var itemButtons	= $('tr:nth-child(3) td', this); 
				if(prodImg != null || widthSelect != null){
          $('.wf-acc-list').append('<div class="wf-prod-item" data-item="wf-item-'+uniqueClass+'">'
            + '<h4>'+ title +'</h4>' 
            + '<div class="wf-prod-img">'+ prodImg +'</div>'
            + '<div class="wf-prod-desc"><ul>'+ prodDesc +'</ul></div>'
            + '<div class="wf-order-actions">'
              + '<div class="wf-row">'
                + '<div class="wf-product-width"><small>Measurement</small>'+ widthSelect +'</div>'	
                + '<div class="wf-product-hinge"><small>Hinge</small>'+ hingeSelect +'</div>'
                + '<div class="wf-product-end"><small>Finished End</small>'+ endSelect +'</div>'
                + '<div class="wf-product-qty"><small>Qty</small>'+ qtyInput +'</div>'
              + '</div>'	
            + '</div>'
            + '<div class="wf-item-actions">'
            +	'<a href="#" class="wf-add-quote">Add to quote</a><a href="#" class="wf-view-quote">View quote</a>'
            + '</div>'
          +'</div>'); 
          uniqueClass++;
        }
			});
      
      $('.wf-prod-item').each(function(){
        var theSpan = $('.wf-product-width span', this);
        
        if(theSpan.text() == 'N/A' || theSpan.text() == ''){
          theSpan.text('');
          console.log('span: '+theSpan.text());
        }
        $('.wf-row', this).after(theSpan);
      });
		}
		function updateColorSelection(){
			$('#StandardList .StandardImg img').each(function(){
				var getColor = $(this).siblings('div').html().replace('<br>', ' ');
				var getColorData = $(this).siblings('div').html().replace('<br>', '-');
				$(this).attr('data-color', getColorData);
				var imgSrc  = $(this).attr('src');
				var regEx	= new RegExp('_on', 'gi');
				if(regEx.test(imgSrc)){
					imgSrc = imgSrc.replace('_on', '_off');
					$('.wf-standard-paint').append('<a href="#" class="wf-img-mask active" id="'+getColorData+'">'
					+	'<span><img src="'+imgSrc+'" /></span><p>'+getColor+'</p></a>');
				} else {
					$('.wf-standard-paint').append('<a href="#" class="wf-img-mask" id="'+getColorData+'">'
					+	'<span><img src="'+imgSrc+'" /></span><p>'+getColor+'</p></a>');				
				}
			});
			
			if($('#designerpaint').children().length > 0){
				$('#designerpaint .StandardImg img').each(function(){
					var getColor = $(this).siblings('div').html().replace('<br>', ' ');
					var getColorData = $(this).siblings('div').html().replace('<br>', '-');
					$(this).attr('data-color', getColorData);
					var imgSrc  = $(this).attr('src');
					var regEx	= new RegExp('_on', 'gi');
					if(regEx.test(imgSrc)){
						imgSrc = imgSrc.replace('_on', '_off');
						$('.wf-designer-paint').append('<a href="#" class="wf-img-mask active" id="'+getColorData+'">'
								+	'<span><img src="'+imgSrc+'" /></span><p>'+getColor+'</p></a>');	
					} else {
						$('.wf-designer-paint').append('<a href="#" class="wf-img-mask" id="'+getColorData+'">'
							+	'<span><img src="'+imgSrc+'" /></span><p>'+getColor+'</p></a>');
					}	
				});
				if(!$('small.has-designer').length > 0){
					$('.wf-designer-paint').before('<small class="has-designer">Designer Paint</small>');
				}	
			} else {
				$('small.has-designer').remove();
			}			
		}
		
		function updateDoorSelection(){
 
			var arrowPos = $(document).find('.wf-door-item.active').attr('data-door');
			$('.wf-arrow_box').attr('class', 'wf-arrow_box '+arrowPos);
			$('.wf-door-detail h2').empty();
			$('.wf-door-detail ul').empty();
			var serialVal = $('#serial_value').text();
			$('.wf-door-detail h2').append(serialVal);
			var colorVal = $('#color_value').text();
			$('.wf-door-detail ul').append('<li>'+colorVal+'</li>');
			$('ul#desc_value li').each(function(){ 
				var detailList = $(this).clone();
				$(detailList).appendTo('.wf-door-detail ul') 
			});
		}
	},
		
	init: function(){
    
    var foo = $('#serial_value').text();
    if(foo != ''){
         this.addCssProps();
         this.editContent(); 
       } else {
         setTimeout(function(){	
           WF.exp.init();
        }, 500);
     }
    
		
	}	
};  
WF.exp.init();