/* source files for dev cycle, add this to the optimizely experiment so you can work from your ide
 * 
$('head').append('<link href="http://projects.widerfunnel.com/CLIENTS/dan-dev/style.css" type="text/css" rel="stylesheet">');
$(function(){var fileRef = document.createElement('script'); fileRef.setAttribute("type","text/javascript"); fileRef.setAttribute("src", "http://projects.widerfunnel.com/CLIENTS/dan-dev/wf-script.js"); document.body.appendChild(fileRef);});

 * 
 */

var WF = {};
WF.exp = {
	
	//Add custom member variables for specific projects here
	dockette: '',
	variant: 'A',


	// Add specific CSS or hide elements
	addCssProps: function(){
		$('head').append('');
	},

	//Modify Content
	editContent: function(){	
		$(document).ready(function(){
			
		});	
	},
	
	bindEvents: function(){
		
	},
	
	init: function(){
		this.addCssProps();
		this.editContent();
		this.bindEvents(); 
	}	
};  
WF.exp.init();